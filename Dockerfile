# We have to be using Rawhide to use chained builds
# See: https://docs.fedoraproject.org/en-US/package-maintainers/Using_the_Koji_Build_System/#chained_builds
FROM registry.fedoraproject.org/fedora:rawhide

# Install build dependencies
# See: https://docs.fedoraproject.org/en-US/packaging-guidelines/Rust/
RUN dnf install -y mock nano rpm-build rust-packaging rust2rpm

# Create and enter build directory, after populating it with our sources
RUN mkdir -p /root/rpmbuild
COPY SOURCES /root/rpmbuild/SOURCES
WORKDIR /root/rpmbuild/SOURCES

# Install build-deps to build acl-sys SRPM
RUN dnf install -y libacl-devel rust-libc-devel

# Install build deps to build posix-acl SRPM
RUN dnf install -y rust-tempfile+default-devel

# Install build-deps to build gperftools SRPM
RUN dnf install -y rust-error-chain+default-devel rust-lazy_static+default-devel rust-pkg-config+default-devel gperftools-devel

# Install build deps to build LAUREL SRPM
RUN dnf install -y rust-bencher+default-devel rust-bindgen+default-devel rust-caps+default-devel rust-getopts+default-devel rust-indexmap+default-devel rust-nix+default-devel rust-nom+default-devel rust-regex+default-devel rust-serde+default-devel rust-serde+derive-devel rust-serde_json+default-devel rust-serde_json+preserve_order-devel rust-toml+default-devel

# Then run:
#   - cd /root/rpmbuild/SOURCES
#   - rpmbuild -ba rust-acl-sys.spec
#   - cd /root/rpmbuild/SRPMS
#   - cd /root/rpmbuild/RPMS/noarch
#   - dnf localinstall -y rust-acl-sys*
#   - cd /root/rpmbuild/SOURCES
#   - rpmbuild -ba rust-posix-acl.spec
#   - cd /root/rpmbuild/RPMS/noarch
#   - dnf localinstall -y rust-posix-acl*
#   - cd /root/rpmbuild/SOURCES
#   - rpmbuild -ba rust-gperftools.spec
#   - cd /root/rpmbuild/RPMS/noarch
#   - dnf localinstall -y rust-gperftools*
#   - cd /root/rpmbuild/SOURCES
#   - rpmbuild -ba rust-laurel.spec
