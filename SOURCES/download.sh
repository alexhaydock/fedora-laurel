#!/bin/sh

# Create SPEC files for our rust crates that are dependencies
# of posix-acl and are not currently packaged in Fedora
#
# After downloading this source, I add the following line
# to satisfy an undeclared dependency:
#
# BuildRequires: libacl-devel
rust2rpm -s acl-sys

# Create SPEC files for our rust crates that are dependencies
# of LAUREL and are not currently packaged in Fedora
rust2rpm -s gperftools
rust2rpm -s posix-acl

# Create SPEC file for LAUREL
rust2rpm -s laurel
