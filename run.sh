#!/bin/sh
set -xeu
podman build -t laurel .
# We need the --privileged flag in order for mock to use unshare
# See: https://github.com/rpm-software-management/mock/issues/403
podman run --rm -it --privileged localhost/laurel
